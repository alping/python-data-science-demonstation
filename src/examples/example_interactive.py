# %% [markdown]
# # Example Python Script
# This example script can be run in interactive mode

# %%
# Import packages
import numpy as np
import pandas as pd
import statsmodels.formula.api as smf

# %%
# Create example data
n = 1000
b0, b1, b2 = 2, 5, 1

x = np.random.random(size=n) * 6 - 3
y = np.random.normal(loc=b0 + b1 * x + b2 * x**2, scale=1, size=n)

data = pd.DataFrame({"x": x, "y": y})

# %%
# Inspect data
data.plot.scatter("x", "y", size=1)

# %%
# Fit data to linear model
fit = smf.ols("y ~ x + I(x ** 2)", data).fit()
fit.summary()

# %%
# Inspect data fit
fb0, fb1, fb2 = fit.params
fx = np.linspace(-3, 3)
fy = b0 + b1 * fx + b2 * fx**2

ax = data.plot.scatter("x", "y", size=1)
ax.plot(fx, fy, color="red")
